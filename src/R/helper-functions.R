#' Read gmt file.
#'
#' Reads a geneset file in gmt format to a list.
#'
#' @param filename Path to file to read
#' @param nmin Interger. Only retain geneset with >= nmin genes.
#' @param description: Bool. default is FALSE. If set TRUE, the second column of
#' the gmt file is added to the names of the list items, seperated by an @.
#'
#' @return a list. List item names are geneset names, items are vectors of
#' geneset members.
#' @export
#'
read_gmt <- function(file, nmin = 0, description = FALSE) {
    gs <- scan(file, what = "", sep = "\n") %>% strsplit("\t")
    if (description == FALSE) {
        names(gs) <- sapply(gs, function(x) x[1])
    } else {
        names(gs) <- sapply(gs, function(x) paste(x[1], x[2], sep = '@'))
    }

    gs <- gs %>%
        lapply('[', -1) %>% lapply('[', -1) %>%
        .[lapply(., length) >= nmin]
    return(gs)
}



#' Write list to gmt file.
#'
#' Writes a list with geneset to gmt format. List item names are used as both
#' geneset name and geneset description.
#'
#' @param lst. List constaining genesets
#' @param file. path to file to write to
#' @param nmin Interger. Only retain geneset with >= nmin genes.
#' @export
#'
write_gmt <- function(lst, file, nmin=0){

    f <- file(file)
    x <- character()
    for (set_name in names(lst)) {
        # Remove NAs,
        set <- lst[[set_name]]
        set <- set[!is.na(set)]
        x <- c(x, stringr::str_c(c(set_name, set_name, set), collapse = '\t'))
    }
    writeLines(x, f)
    close(f)
}


# Get the scaled expression values in a tidy data frame
get_scaled_df <- function(eset, scale = F){
  scale_exprs <- t(scale(t(exprs(eset)),  scale = scale)) %>%
    as.data.frame() %>%
    tibble::rownames_to_column() %>%
    tibble::as_tibble()

  clus_dat <- fData(eset) %>%
    tibble::rownames_to_column() %>%
    tibble::as_tibble() %>%
    dplyr::select(rowname, cluster)

  df <- full_join(scale_exprs, clus_dat, by = "rowname") %>%
    tidyr::gather(label, value, -rowname, -cluster) %>%
    # Hacky solution to deal with naming inconsistencies between mRNA, prot
    dplyr::mutate(label =  str_replace(label, "UNSTIM", "CTR")) %>%
    tidyr::separate(label, c("cell", "treat", "time")) %>%
    tidyr::unite(label, cell, treat, remove = F) %>%
    tidyr::unite(group, rowname, label, remove = F) %>%
    dplyr::mutate(time = as.integer(time)) %>%
    dplyr::mutate(label = factor(
      label,
      levels = c("W_CTR", "W_G",  "W_P","W_PG", "B32_CTR", "B32_P"))
    )
  return(df)
}



#' Plot time series profile of multiple features.
#'
#' @param eset An ExpressionSet object with mutltiple feature. fData should have
#' column with clutering results. Intensities are mean-centered and scaled to
#' have the same sd.
#' @param clust Indicate which cluster to plot. If none given, all are plotted.
#' @param alpha alpha value of the 95% condidence interval
#' @param size Size of the cluster-mean line
#' @param scale Bool: Scale variance?
#'
#' @export

cgc_clusterplot <- function(eset, clust = NULL, alpha = 0.05, size = 1,
  scale = F, ncol = NULL, mean_cl = TRUE){

  # scale_exprs <- t(scale(t(exprs(eset)),  scale = scale)) %>%
  #   as.data.frame() %>%
  #   tibble::rownames_to_column() %>%
  #   tibble::as_tibble()
  #
  # clus_dat <- fData(eset) %>%
  #   tibble::rownames_to_column() %>%
  #   tibble::as_tibble() %>%
  #   dplyr::select(rowname, cluster)
  #
  # df <- full_join(scale_exprs, clus_dat, by = "rowname") %>%
  #   tidyr::gather(label, value, -rowname, -cluster) %>%
  #   # Hacky solution to deal with naming inconsistencies between mRNA, prot
  #   dplyr::mutate(label =  str_replace(label, "UNSTIM", "CTR")) %>%
  #   tidyr::separate(label, c("cell", "treat", "time")) %>%
  #   tidyr::unite(label, cell, treat, remove = F) %>%
  #   tidyr::unite(group, rowname, label, remove = F) %>%
  #   dplyr::mutate(time = as.integer(time)) %>%
  #   dplyr::mutate(label = factor(
  #     label,
  #     levels = c("W_CTR", "W_G",  "W_P","W_PG", "B32_CTR", "B32_P"))
  #   )
  df <- get_scaled_df(eset, scale = scale)

  if (!is.null(clust)) {
    stopifnot(is.numeric(clust) & length(clust) == 1)
    df <- filter(df, clust == cluster)
  }

  # df_mean <- df %>%
  #   dplyr::group_by(cluster, label, time) %>%
  #   dplyr::summarise(value = median(value, na.rm = TRUE))

  cols <- c("#636363", "#1f77b4", "#d6616b", "#9467bd", "#AEC7E8", "#D0A9F5")


  p <- ggplot(df, aes(x = time, y = value,  color = label, fill = label)) +
    ggplot2::stat_summary(fun.y = "mean", geom = "line", size = size)

  if (mean_cl) {
    p <- p + ggplot2::stat_summary(fun.data = "mean_cl_boot", geom = "ribbon",
      alpha = I(alpha), aes(linetype = NA))
  } else{
    p <- p + ggplot2::geom_line(alpha = alpha, size = 0.1, aes(group = group))
  }



  p <- p +
    ggplot2::scale_color_manual(values = cols) +
    ggplot2::scale_fill_manual(values = cols) +
    ggplot2::xlab("Time [h]") +
    ggplot2::ylab("Mean scaled intensity")
  # p <- ggplot() +
  #   # ggplot::geom_smooth(aes(fill = label)) +
  #   ggplot2::geom_line(
  #     data = df,
  #     aes(x = time, y = value, color = label, group = group),
  #     alpha = alpha, size = minorsize
  #     ) +
  #   ggplot2::geom_line(data = df_mean,
  #                      aes(x = time, y = value, color = label),
  #                      size = size) +
  #   ggplot2::scale_color_manual(values = cols) +
  #   xlab("Time [hr]") +
  #   ylab("Scaled intensities")

  if (is.null(clust)) {
    p <- p + ggplot2::facet_wrap(~cluster, ncol = ncol)
  }

  return(p)

}

#' Get counts per million of a count matrix
#'
#' @param mat - a matrix of features by samples
#' @return - the normalized matrix
#' @export
get_cpm <- function(mat){sweep(mat, 2, colSums(mat)/1e6, "/")}


#' Remove lowly expressed genes from data set
#'
#' Filtering step to reduce computation and possibly multiple testing.
#' For DESeq2, Indendent filtering is performed by the result fuction (see DESeq2 vignette)
#' Genes are filtered out if not at least ncol_min samples have an counts-per-milion of > cpm_cutoff
#'
#'
#' @param x - data set, either an ExpressionSet, a matrix (rows = genes), or an DESeqDataSet.
#' @param cpm_cuttoff - minimal number of cpm
#' @param ncol_min - minimal number of columns which the gene has at least the CPM cutoff
#' @export
#'
remove_low_count_genes <- function(x, cpm_cutoff, ncol_min){
  if (class(x) == "ExpressionSet") {
    x[rowSums(get_cpm(Biobase::exprs(x)) > cpm_cutoff) >= ncol_min, ]
  } else if (class(x) == "matrix") {
    x[rowSums(get_cpm(x) > cpm_cutoff) >= ncol_min, ]
  } else if (class(x) == "DESeqDataSet") {
    x[rowSums(get_cpm(DESeq2::counts(x)) > cpm_cutoff) >= ncol_min, ]
  }  else{
    stop('remove_low_count_genes: invalid input; it has to be either "matrix", "ExpressionSet" or "DESeqDataSet".')
  }
}


