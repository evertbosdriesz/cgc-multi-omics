# Description

This repository contains the scripts accompanying the publication "A system-wide approach to monitor responses to synergistic BRAF and EGFR inhibition in colorectal cancer cells"

## Contents

### Data

* The proteomics, phosphoproteomics, and RNA subdirectory contain quantified proteins/phospho-proteins/reads.
* The external subdirectory contains.
  * networKIN predictions, used for kinase-substrate enrichment analysis. 
  * Genesets obtained from MSigDB

### Results

* The RData subfolder contains intermediate data files
* The clustering and differential expression subfolders contain analysis results.

### scr/R

All scripts and RMarkdown notebooks that were used to perform the analyses.